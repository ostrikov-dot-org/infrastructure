
locals {
  # if exists - good, if not terragrunt will prompt to create one.
  tg_bucket_name    = get_env("TG_BUCKET_NAME", "ostrikov.org-tfstate")
  tg_bucket_region  = get_env("TG_BUCKET_REGION", "eu-north-1")
  tg_default_region = get_env("TG_DEFAULT_REGION", "eu-north-1")
}

remote_state {
  backend = "s3"
  generate = {
    path      = "__terragrunt_backend.tf"
    if_exists = "overwrite_terragrunt"
  }
  config = {
    bucket  = local.tg_bucket_name
    key     = "${path_relative_to_include()}/terraform.tfstate"
    region  = local.tg_bucket_region
    encrypt = true
    # can also add dynamodb lock, and you should if there is multiple
    # engineers working at the same repo.
  }
}

# locals
# https://terragrunt.gruntwork.io/docs/getting-started/quick-start/#example
#
# inputs
# https://terragrunt.gruntwork.io/docs/features/inputs/#variable-precedence
#
# so overwrite default with your own input if you need a different region

inputs = {
  aws_region = local.tg_default_region
}

generate "provider_common" {
  path      = "__terragrunt_config.tf"
  if_exists = "overwrite_terragrunt"

  # Note that we are using terragrunts local, so extra " pair is a must.
  contents = <<-EOF
    provider "aws" {
      region = var.aws_region
    }

    variable "aws_region" {
      type = string
    }
  EOF
}