include "root" {
  path = find_in_parent_folders()
}

terraform {
  source = "tfr:///cloudposse/cloudfront-s3-cdn/aws?version=0.93.0"
}

inputs = {
  # https://github.com/cloudposse/terraform-aws-cloudfront-s3-cdn
  # will create and manage bucket

  name              = "ostrikov-dot-org"
  aliases           = ["ostrikov.org"]
  dns_alias_enabled = true
  parent_zone_name  = "ostrikov.org"

  deployment_principal_arns = {
    "arn:aws:iam::407616334071:user/ostrikov.org-delivery" = [""]
  }

  acm_certificate_arn = "arn:aws:acm:us-east-1:407616334071:certificate/1c8729bc-613f-4613-adfa-d7191a5f0df6"

  cloudfront_access_logging_enabled   = false
  cloudfront_access_log_create_bucket = false
}